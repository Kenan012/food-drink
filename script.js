/* $(document).ready(function(){

let PElement = $("p").text();
    console.log(PElement)

});
*/
$(document).ready(function(){
    // SHOW and HIDE buttons
    $("#hide").click(function(){
        console.log('HIDE before', $('p').is(':hidden'), $('p').css('display'))
        $("p").hide();
        $("p").css("background-color", "");
        console.log('HIDE after', $('p').is(':hidden'), $('p').css('display'))
    })
    $("#show").click(function(){
        console.log('SHOW before', $('p').is(':hidden'), $('p').css('display'))
        $("p").show();
        $("p").css("background-color", "red");
         console.log('SHOW after', $('p').is(':hidden'), $('p').css('display'))
    });
    
    //TOGGLE button
     $("#click").click(function(){
       console.log('Toggle before', $('p').is(':hidden'), $('p').css('display'))
       $("p").toggle();
       console.log('Toggle after', $('p').is(':hidden'), $('p').css('display'))
    });
    
    //Klikni da se priakze novi element
    $("#text1").click(function(){
        $("#text2").slideToggle("slow");
    }); 
    
    //Console log koji prikazuje selektovanje samo sedmog elemnta od elementa koji ima u sebi id Galerija a to je div koji sadrži 9 img elemenata
    console.log($("#Galerija").find("*")[6]);
    
    //Selektovanje svih slika u Galeriji
    
    $(".slano, .slatko, .pice").hover(function(){
        console.log("Kliknuti element je", this);
        //$(this).css("height", "200px").css("width", "250px")
        $(this).css({
            "height": "200px",
            "width": "250px"
        });
    });
    
   $(".slano, .slatko, .pice").mouseleave(function(){
        console.log("Kliknuti element je", this);
        //$(this).css("height", "200px").css("width", "250px")
        $(this).css({
            "height": "150px",
            "width": "200px"
        });
    });
    
    /* $("#broadcast").click(function(){
    setTimeout(function () { activityMonitor.sendActivity('broadcast') }, 2000);
    });
    */
    
    //Selektovanje samo slanih jela 
    $("#odaberiSlano").click(function(){
        $(".slano").show();
        $(".slatko").hide();
        $(".pice").hide();
        setTimeout(function(){ 
        $(".slatko").show();
        $(".pice").css("display", "block") }, 5000);
    })
    
    //Selektovanje samo slatkih jela 
    $("#odaberiSlatko").click(function(){
        $(".slatko").show();
        $(".slano").hide();
        $(".pice").hide();
        setTimeout(function(){
        $(".slano").show();
        $(".pice").css("display", "block") }, 5000);
    })
    
    //Selektovanje samo pića
    $("#odaberiPice").click(function(){
        $(".pice").show();
        $(".slano").hide();
        $(".slatko").css("display", "none");
        setTimeout(function() {
        $(".slano").show();
        $(".slatko").css("display", "block") }, 5000);   
    })
    
    
    //Vracanje vrijednosti slika nakon 5000ms
    /* setTimeout(function(){
        $(".slano, .slatko, .pice").show();
    }, 5000); */
    
    // Dio za slider u JaviScript
    var myIndex = 0;
        carousel();

    function carousel() {
      var i;
      var x = document.getElementsByClassName("naslovnaslika");
      for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";  
      }
      myIndex++;
      if (myIndex > x.length) {myIndex = 1}    
      x[myIndex-1].style.display = "block";  
      setTimeout(carousel, 2000); 
    }
    
});

// Funkcija vezana za Submit Forme
function Switchforme(){
    console.log("Radi li?")
    var name= document.getElementsByClassName("formazapolja")[0].value;
    var surname= document.getElementsByName("surname")[0].value;
    var address= document.getElementsByName("adress")[0].value;
    var age= document.getElementsByName("age")[0].value;
    var mail= document.getElementsByName("mail")[0].value;
    var date= document.getElementsByName("date")[0].value;
    console.log("unijeto ime je", name, "a unijeto prezime je", surname, "adresa je", address, "email je", mail, "i datum je", date)
    document.getElementById("UneseniPodaci").innerHTML="Uneseno ime je: " + name + "<br/>Uneseno prezime je: " + surname + "<br/>Unesena adresa je: " + address + "<br/>Unesene godine su: " + age + "<br/>Uneseni email je: " + mail +"<br/>Uneseni datum je: " + date;
    
    return false;
} 
 
